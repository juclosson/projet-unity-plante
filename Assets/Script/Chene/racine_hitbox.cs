using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Racine_hitbox : MonoBehaviour
{
    private List<Collider2D> moleculeColliders = new List<Collider2D>();  // Liste des colliders de mol�cules

    void Start()
    {

    }

    void OnTriggerEnter2D(Collider2D other)
    {
        
        if (other.CompareTag("h2o") || other.CompareTag("nutriment"))
        {
            moleculeColliders.Add(other);  // Ajout du collider de la mol�cule � la liste
            //Debug.Log("une molecule ets rentr�");
           
        }
    }

    void OnTriggerExit2D(Collider2D other)
    {
        if (moleculeColliders.Contains(other))
        {
            moleculeColliders.Remove(other);  // Retrait du collider de la mol�cule de la liste
           
            //Debug.Log("La mol�cule a cess� de collid� avec " + gameObject.name);
        }
    }

    public void OnMouseDown()
    {
        Recuperation();
    }




    public void Recuperation()
    { 
        // Ajoutez une condition pour v�rifier si au moins une mol�cule est encore au-dessus de la racine
        if (Time.timeScale != 0 && moleculeColliders.Count > 0)
        {
            List<Collider2D> moleculesToRemove = new List<Collider2D>();

            foreach (var collider in moleculeColliders)
            {
                // V�rifiez le tag de la mol�cule et incr�mentez le compteur appropri�
                if (collider.CompareTag("h2o"))
                {
                    Debug.Log("h2o delete");
                    MoleculeManager2.IncrementH2OCount();
                  
                }
                else if (collider.CompareTag("nutriment"))
                {
                    MoleculeManager2.RecupNutrimentt();
                  
                }

                moleculesToRemove.Add(collider);
            
            }

            // Supprimez les mol�cules de la liste
            foreach (var colliderToRemove in moleculesToRemove)
            {
                moleculeColliders.Remove(colliderToRemove);
                Destroy(colliderToRemove.gameObject);
            }

           
            // Retirez les mol�cules de la liste originale
            moleculeColliders.Clear();
        }
    }
}
