using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Chene_hitbox2 : MonoBehaviour
{
    private bool isSpriteActive = false;
    private SpriteRenderer spriteRenderer;
    private List<Collider2D> moleculeColliders = new List<Collider2D>();  // Liste des colliders de mol�cules

    void Start()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
        isSpriteActive = false;
        UpdateSpriteDisplay();
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("co2")|| other.CompareTag("o2"))
        {
            moleculeColliders.Add(other);  // Ajout du collider de la mol�cule � la liste
            isSpriteActive = true;
            UpdateSpriteDisplay();
          //Debug.Log("La mol�cule est entr�e en collision avec " + gameObject.name);
        }
    }

    void OnTriggerExit2D(Collider2D other)
    {
        if (moleculeColliders.Contains(other))
        {
            moleculeColliders.Remove(other);  // Retrait du collider de la mol�cule de la liste
            if (moleculeColliders.Count == 0)
            {
                isSpriteActive = false;
                UpdateSpriteDisplay();
            }
          //Debug.Log("La mol�cule a cess� de collid� avec " + gameObject.name);
        }
    }
  
    void OnMouseDown()
    {
        // Ajoutez une condition pour v�rifier si au moins une mol�cule est encore au-dessus de la feuille
        if (Time.timeScale != 0 && isSpriteActive && moleculeColliders.Count > 0)
        {
            List<Collider2D> moleculesToRemove = new List<Collider2D>();

            foreach (var collider in moleculeColliders)
            {
                // V�rifiez le tag de la mol�cule et incr�mentez le compteur appropri�
                if (collider.CompareTag("co2"))
                {
                    MoleculeManager2.IncrementCo2Count();
                    //Debug.Log("1 mol�cule de CO2 a �t� supprim�e ");
                }
                else if (collider.CompareTag("o2"))
                {
                    MoleculeManager2.IncrementO2Count();
                    //Debug.Log("1 mol�cule de O2 a �t� supprim�e.");
                }

                moleculesToRemove.Add(collider);
            }

            // Supprimez les mol�cules de la liste
            foreach (var colliderToRemove in moleculesToRemove)
            {
                moleculeColliders.Remove(colliderToRemove);
                Destroy(colliderToRemove.gameObject);
            }

            // Affichez le message de log avec le nombre de mol�cules supprim�es

            // Retirez les mol�cules de la liste originale
            moleculeColliders.Clear();

            // D�sactivez le sprite de la feuille si la liste est maintenant vide
            isSpriteActive = false;
            UpdateSpriteDisplay();
        }
    }


    void UpdateSpriteDisplay()
    {
        if (spriteRenderer != null)
        {
            spriteRenderer.enabled = isSpriteActive;
        }
    }
}

