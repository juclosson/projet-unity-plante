using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Nutriment : MonoBehaviour
{
    [SerializeField]
    private float vitesse = 0.5f;
    [SerializeField]
    private float lifetime = 10.0f;
    private float timer = 0;
    private Rigidbody2D rb;

    void Start()
    {
        rb = gameObject.GetComponent<Rigidbody2D>();

        // Pas besoin de donner la position car c'est la feuille qui la donne au nutriment au moment de l'instancier

        Vector2 direction = new Vector2(0, -1);

        rb.velocity = direction * vitesse;    // Pas de deltatime car c'est le moteur qui calcule
    }

    // Update is called once per frame
    void Update()
    {
        timer += Time.deltaTime;

        if (timer > lifetime)
        {
            Destroy(gameObject);
        }
    }
}
