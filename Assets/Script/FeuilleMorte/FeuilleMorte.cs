using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
//using UnityEditor.EditorTools;
using UnityEngine;
using static UnityEngine.GraphicsBuffer;

public class FeuilleMorte : MonoBehaviour
{
    public Sprite ftombe;
    public Sprite fsol;
    private bool transfo =false;
    [SerializeField]
    private float ySol = -2.1f;
    [SerializeField]
    private float spawnRate = 0.5f;
    [SerializeField]
    private float feuilleDuration = 3f;
    [SerializeField]
    private GameObject NutrimentPrefab;
    [SerializeField]
    private float y0 = 4f;
    

    // Start is called before the first frame update
    void Start()
    {
        //this.gameObject.GetComponent<SpriteRenderer>().sprite = ftombe;

        float x0 = Random.Range(-2f, 1.5f);

        transform.position = new Vector2(x0, y0);

    }

    // Update is called once per frame
    void Update()
    {


        //on fait tomber la feuille:

        if (transform.position.y > ySol)
        {
            transform.position = transform.position + new Vector3(0,-1*Time.deltaTime,0) ;
            RotateLeft();

        }
        else
        //si elle a atteint le sol, on change de sprite et on génère des nutriments
        {
            if(transfo==false)
            { 
                Debug.Log("atteint le sol");
                transformation_nutriment();
                transfo = true;
                StartCoroutine(NutrimentSpawnCoroutine()); 
            }
           
        }
       



    } 
    void RotateLeft()
    {
            transform.Rotate(Vector3.forward * -90* Time.deltaTime);
    }


    void transformation_nutriment() //on remet le sprite dans le bon sens
    {
        Vector3 newRotation = new Vector3(0, 0, 170);
        transform.eulerAngles = newRotation;


        this.gameObject.GetComponent<SpriteRenderer> ().sprite = fsol;

    }

    IEnumerator NutrimentSpawnCoroutine()
    {
        int nbNutri = 0;

        while (nbNutri < feuilleDuration)   // La feuille produit un nombre donné de nutriments puis s'autodétruit
        {
            // La feuille donne sa position au nutriment
            GameObject Nutriment = Instantiate(NutrimentPrefab, gameObject.transform.position, Quaternion.identity);
            
            nbNutri ++;

            yield return new WaitForSeconds(spawnRate);
        }

        Destroy(gameObject);
    }
}
