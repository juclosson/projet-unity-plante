using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FeuilleMorteManager : MonoBehaviour
{
    [SerializeField]
    private GameObject FeuilleMortePrefab;
    public float spawnRate = 11;
    private TimeManager timeManager;
    private int precedentJour = 0;

    // Start is called before the first frame update
    void Start()
    {
        GameObject timeManagerObject = GameObject.Find("TimeManager(Clone)");
        timeManager = timeManagerObject.GetComponent<TimeManager>();
    }

    // Update is called once per frame
    void Update()
    {
        // Commence le deuxième jour (jour = 1 car on commence de 0)
        if ((timeManager.jour==1)   && (precedentJour==0))
        {
            StartCoroutine(FeuilleSpawnCoroutine());
            precedentJour = 1;
        }

        precedentJour = timeManager.jour;
    }

    IEnumerator FeuilleSpawnCoroutine()
    {
        while (true)
        {
            GameObject FeuilleMorte = Instantiate(FeuilleMortePrefab, Vector3.zero, Quaternion.identity);
            
            yield return new WaitForSeconds(spawnRate);
        }
    }
}
