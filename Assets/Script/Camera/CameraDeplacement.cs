using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraDeplacement : MonoBehaviour
{
    private float testDeplacement;
    public float deplacementVertical;
    
    [SerializeField]
    private float ymax = 7;
    [SerializeField]
    private float ymin = -5;

    /*  // Commenté car le scroll souris a été retiré en raison de problèmes
    [SerializeField]
    private float vitesseMouse = 100f;
    */
    [SerializeField]
    private float vitesseKey = 3.0f;

    private int level;

    public bool scrollEnabled = true;

    // Start is called before the first frame update
    void Start()
    {
        GameObject LevelManagerObject = GameObject.Find("LevelManager(Clone)");
        if (LevelManagerObject == null)
        {
            Debug.Log("levelManager not found");
        }

        LevelManager levelManager = LevelManagerObject.GetComponent<LevelManager>();
        level = levelManager.level;

        GameObject Sol = GameObject.Find("Sol(Clone)");
        if (Sol == null)
        {
            Debug.Log("Sol not found");
        }

        // Choix de la hauteur du sol et des valeurs limite de la caméra

        if (level == 1)
        {
            Sol.transform.position = new Vector3(0.0f, -2.7f, 0.0f);
        }
        else if (level == 2) 
        {
            Sol.transform.position = new Vector3(0.0f, -4.4f, 0.0f);
            ymin = -2.5f;
            ymax = 7f;
        }
        else if (level == 3)
        {
            Sol.transform.position = new Vector3(0.0f, -4.7f, 0.0f);
            ymin = -5f;
            ymax = 9f;
        }

        deplacementVertical = 0;
    }

    void Update()
    {
        {
            // scrollEnabled permet de savoir si on a le droit de scroll, ce n'est pas le cas lorsqu'il y a un scroll automatique pour l'affichage des règles
            if (level > 1 && scrollEnabled)
            {
                // float mouseScroll = Input.GetAxis("Mouse ScrollWheel");  // Commenté pour la même raison que ci-dessous
                float keyScroll = Input.GetAxis("Vertical");
                
                /*
                    // Permet de faire un scroll molette ou flèches
                    // Commenté car le scroll molette fonctionne mal, la limite haute et basse n'est pas la même que le scroll clavier
                    // et nous n'avons plus le temps de régler ça
                if (mouseScroll != 0)
                {
                    testDeplacement = mouseScroll * vitesseMouse;
                }
                else
                {
                    testDeplacement = keyScroll * vitesseKey;
                }
                */
                
                testDeplacement = keyScroll * vitesseKey;   // Remis ici seul

                // Problème vers le haut : lorsqu'on a atteint une limite, en relachant le bouton on peut encore monter. Ainsi on peut encore monter jusqu'à une seconde
                // limite en appuyant plusieurs fois
                if ((testDeplacement > 0 && transform.position.y + testDeplacement < ymax) || (testDeplacement < 0 && transform.position.y - testDeplacement > ymin))
                {
                    deplacementVertical = testDeplacement;
                    transform.Translate(Vector3.up * deplacementVertical * Time.deltaTime);
                }
                else
                {
                    deplacementVertical = 0;
                }
            }
        }
    }



}
