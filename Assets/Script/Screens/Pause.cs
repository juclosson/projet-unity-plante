using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Pause : MonoBehaviour
{
    [SerializeField]
    private GameObject PauseButtonCanvasPrefab;
    [SerializeField]
    private GameObject PauseMenuCanvasPrefab;
    GameObject PauseMenuCanvas;
    GameObject pauseButton;
    GameObject resumeButton;

    // Start is called before the first frame update
    void Start()
    {
        GameObject PauseButtonCanvas = Instantiate(PauseButtonCanvasPrefab);
        PauseMenuCanvas = Instantiate(PauseMenuCanvasPrefab);

        pauseButton = GameObject.Find("PauseButtonCanvas(Clone)/PauseButton");
        pauseButton.GetComponent<Button>().onClick.AddListener(PauseOnClick);

        resumeButton = GameObject.Find("PauseMenuCanvas(Clone)/ResumeButton");
        resumeButton.GetComponent<Button>().onClick.AddListener(ResumeOnClick);

        GameObject.Find("PauseMenuCanvas(Clone)/MenuButton").GetComponent<Button>().onClick.AddListener(MenuOnClick);
        
        PauseMenuCanvas.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    
    void PauseOnClick()
    {
        if (Time.timeScale == 1)
        { 
            Time.timeScale = 0; // Arrête le temps, met le jeu en pause
            PauseMenuCanvas.SetActive(true);
            pauseButton.SetActive(false);
        }
    }

    void ResumeOnClick()
    {
        Time.timeScale = 1; // Remet le jeu en route
        PauseMenuCanvas.SetActive(false);
        pauseButton.SetActive(true);
    }

    void MenuOnClick()
    {
        Time.timeScale = 1;
        UnityEngine.SceneManagement.SceneManager.LoadScene("Main_menu");
    }
}
