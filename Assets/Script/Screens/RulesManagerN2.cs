using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class RulesManagerN2 : MonoBehaviour
{
    [SerializeField]
    private float waitTime = 1.5f;
    [SerializeField]
    private GameObject RulesCanvasPrefab;
    private GameObject RulesCanvas;
    private Canvas rulesRenderer;
    private TextMeshProUGUI rulesText;

    [SerializeField]
    private GameObject ContinueButtonCanvasPrefab;
    private GameObject continueButton;
    private Canvas continueButtonRenderer;

    private TimeManager timeManager;
    private GameObject pauseButtonCanvas;
    private GameObject GlucoseJauge;
    private GameObject H2OJauge;
    private GameObject MainCamera;
    private GameObject BarresCanvas;
    
    private Renderer H2ORenderer;
    private Renderer FeuilleMorteRenderer;

    private string H2ORulesString1;
    private string H2ORulesString2;
    private string NutrimentRulesString;
    private int precedentJour = 0;
    [SerializeField]
    private float scrollVitesse = 3f;

    private CameraDeplacement CameraDeplacement;

    private enum Rules {Eau1, Eau2, Nutriment};
    private Rules ruleNumber = 0;

    // Start is called before the first frame update
    void Start()
    {   
        GameObject timeManagerObject = GameObject.Find("TimeManager(Clone)");
        timeManager = timeManagerObject.GetComponent<TimeManager>();

        // Récupération des objets dont on modifiera l'affichage
        pauseButtonCanvas = GameObject.Find("PauseButtonCanvas(Clone)");

        BarresCanvas = GameObject.Find("BarresCanvas chap2(Clone)");

        // Instantiation du RulesCanvas
        MainCamera = GameObject.Find("Main Camera(Clone)");

        CameraDeplacement = MainCamera.GetComponent<CameraDeplacement>();

        RulesCanvas = Instantiate(RulesCanvasPrefab, MainCamera.GetComponent<Transform>());
        rulesRenderer = RulesCanvas.GetComponent<Canvas>();
        rulesRenderer.enabled = false;

        GameObject ContinueButtonCanvas = Instantiate(ContinueButtonCanvasPrefab);
        continueButtonRenderer = ContinueButtonCanvas.GetComponent<Canvas>();
        continueButtonRenderer.enabled = false;

        GameObject H2O = GameObject.Find("H2O(Clone)");
        if (H2O == null)
        {
            Debug.Log("H2O not found");
        }
        H2ORenderer = H2O.GetComponent<Renderer>();

        GlucoseJauge = GameObject.Find("RulesCanvasN2(Clone)/barre_glucose_regles");
        if (GlucoseJauge == null)
        {
            Debug.Log("GlucoseJauge not found");
        }
        GlucoseJauge.SetActive(false);

        H2OJauge = GameObject.Find("RulesCanvasN2(Clone)/barre_h2o_regles");
        if (H2OJauge == null)
        {
            Debug.Log("H2OJauge not found");
        }
        H2OJauge.SetActive(false);

        // Actions sur les objets du canvas
        continueButton = GameObject.Find("ContinueButtonCanvas(Clone)/ContinueButton");
        continueButton.GetComponent<Button>().onClick.AddListener(ContinueOnClick);

        GameObject rulesTextObject = GameObject.Find("RulesText");
        if (rulesTextObject == null)
        {
            Debug.Log("RulesText not found");
        }

        rulesText = rulesTextObject.GetComponent<TextMeshProUGUI>();
        if (rulesText == null)
        {
            Debug.Log("RulesText doesn't have a TextMeshProUGUI component");
        }

        H2ORulesString1 = "Il pleut ! La plante a également besoin d’eau pour survivre.\nRécolte les molécules d’eau tombées sur le sol en cliquant sur les racines.";
        H2ORulesString2 = "Utilise les flèches du clavier pour te déplacer tout au long de la plante (▲ vers le haut ou ▼ vers le bas). \nN'oublie pas les autres molécules dont la plante a besoin !";
        NutrimentRulesString = "Regarde, une feuille morte est en train de tomber.\nUne fois au sol, elle se décomposera en nutriments (nourriture) que tu pourras récupérer par tes racines pour aider la plante à produire du glucose.";

        StartCoroutine(DisplayRules(H2ORulesString1, -3));
    }

    // Update is called once per frame
    void Update()
    {
        if (timeManager.jour == 1 && precedentJour == 0)
        {
            StartCoroutine(DisplayRules(NutrimentRulesString, 0));
        }
        precedentJour = timeManager.jour;
    }

    IEnumerator DisplayRules(string rulesString, float yScroll)
    {
        yield return new WaitForSeconds(waitTime);

        // Se décaler vers le y voulu
        Vector3 target = new Vector3(MainCamera.transform.position.x, yScroll, MainCamera.transform.position.z);
        Vector3 targetJauges = new Vector3(BarresCanvas.transform.position.x, yScroll, BarresCanvas.transform.position.z);

        CameraDeplacement.scrollEnabled = false;

        while ((Vector3.Distance(MainCamera.transform.position, target) > 0.001f))
        {
            MainCamera.transform.position = Vector3.MoveTowards(MainCamera.transform.position, target, scrollVitesse * Time.deltaTime);   // voir step
            BarresCanvas.transform.position = Vector3.MoveTowards(BarresCanvas.transform.position, targetJauges, scrollVitesse * Time.deltaTime);

            yield return null;
        }
        
        yield return new WaitForSeconds(0.2f);
        
        CameraDeplacement.scrollEnabled = true;

        Time.timeScale = 0;

        // Affichage du Canvas
        rulesText.SetText(rulesString);

        rulesRenderer.enabled = true;
        continueButtonRenderer.enabled = true;
        pauseButtonCanvas.SetActive(false);

        // Mettre les objets voulus au premier plan

        // Pour le premier jour : H2O
        if (ruleNumber == Rules.Eau1)
        {
            continueButton.transform.position += new Vector3(0, 50, 0);
            // mettre H2O au premier plan
            H2ORenderer.sortingOrder = 3;
            // jauge H2O active
            H2OJauge.SetActive(true);
        }

        // Pour le deuxième jour : nutriment
        else if (ruleNumber == Rules.Nutriment)
        {   
            continueButton.transform.position -= new Vector3(0, 70, 0);

            // On cherche la feuille et les nutriments une fois qu'ils existent -> nutriment pas encore
            GameObject FeuilleMorte = GameObject.Find("FeuilleMorte(Clone)");
            if (FeuilleMorte == null)
            {
                Debug.Log("FeuilleMorte not found");
            }
            FeuilleMorteRenderer = FeuilleMorte.GetComponent<Renderer>();

            // mettre feuille et nutriments au 1er plan
            FeuilleMorteRenderer.sortingOrder = 3;

            // mettre jauge glucose au premier plan
            GlucoseJauge.SetActive(true);
        }
        
    }

    void ContinueOnClick()
    {
        if (ruleNumber == Rules.Eau1)
        {
            rulesText.SetText(H2ORulesString2);
        }
        else 
        {
            Time.timeScale = 1;
            rulesRenderer.enabled = false;
            continueButtonRenderer.enabled = false;
            pauseButtonCanvas.SetActive(true);

            // Mettre tous au 2nd plan
            if (ruleNumber == Rules.Eau2)
            {
                // mettre H2O au 2nd plan
                H2ORenderer.sortingOrder = 1;
                // jauge H2O non active
                H2OJauge.SetActive(false);
            }

            else if (ruleNumber == Rules.Nutriment)
            {
                // mettre feuille et nutriments au 2nd plan
                FeuilleMorteRenderer.sortingOrder = 0;
                // mettre jauge glucose au 2nd plan
                GlucoseJauge.SetActive(false);
            }
        }
        

        ruleNumber ++;
    }

}