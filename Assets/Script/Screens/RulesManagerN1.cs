using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class RulesManagerN1 : MonoBehaviour
{
    [SerializeField]
    private float waitTime = 1f;
    [SerializeField]
    private GameObject RulesCanvasPrefab;
    private GameObject RulesCanvas;
    private Canvas rulesRenderer;
    private TextMeshProUGUI rulesText;

    [SerializeField]
    private GameObject ContinueButtonCanvasPrefab;
    private Canvas continueButtonRenderer;

    private TimeManager timeManager;
    private GameObject pauseButtonCanvas;
    private Renderer CO2Renderer;
    private Renderer O2Renderer;
    private GameObject CO2Jauge;
    private GameObject O2Jauge;
    private GameObject GlucoseJauge;

    private string DayRulesString1;
    private string DayRulesString2;
    private string DayRulesString3;
    private string NightRulesString;
    private string GlucoseRulesString;
    private bool precedenteNuit = false;
    private int precedentJour = 0;
    private Rules ruleNumber = 0;

    private enum Rules {Day1, Day2, Day3, Night, Glucose};

    // Start is called before the first frame update
    void Start()
    {   
        GameObject timeManagerObject = GameObject.Find("TimeManager(Clone)");
        timeManager = timeManagerObject.GetComponent<TimeManager>();

        // Récupération des objets dont on modifiera l'affichage
        pauseButtonCanvas = GameObject.Find("PauseButtonCanvas(Clone)");

        // Instantiation du RulesCanvas
        GameObject MainCamera = GameObject.Find("Main Camera(Clone)");

        RulesCanvas = Instantiate(RulesCanvasPrefab, MainCamera.GetComponent<Transform>());
        rulesRenderer = RulesCanvas.GetComponent<Canvas>();
        rulesRenderer.enabled = false;

        GameObject ContinueButtonCanvas = Instantiate(ContinueButtonCanvasPrefab);
        continueButtonRenderer = ContinueButtonCanvas.GetComponent<Canvas>();
        continueButtonRenderer.enabled = false;
        
        GameObject CO2 = GameObject.Find("CO2(Clone)");
        if (CO2 == null)
        {
            Debug.Log("CO2 not found");
        }
        CO2Renderer = CO2.GetComponent<Renderer>();

        CO2Jauge = GameObject.Find("RulesCanvasN1(Clone)/barre_co2_regles");
        if (CO2Jauge == null)
        {
            Debug.Log("CO2Jauge not found");
        }
        CO2Jauge.SetActive(false);

        O2Jauge = GameObject.Find("RulesCanvasN1(Clone)/barre_o2_regles");
        if (O2Jauge == null)
        {
            Debug.Log("O2Jauge not found");
        }
        O2Jauge.SetActive(false);

        GlucoseJauge = GameObject.Find("RulesCanvasN1(Clone)/barre_glucose_regles");
        if (GlucoseJauge == null)
        {
            Debug.Log("GlucoseJauge not found");
        }
        GlucoseJauge.SetActive(false);

        // Actions sur les objets du canvas
        GameObject continueButton = GameObject.Find("ContinueButtonCanvas(Clone)/ContinueButton");
        continueButton.GetComponent<Button>().onClick.AddListener(ContinueOnClick);

        GameObject rulesTextObject = GameObject.Find("RulesText");
        if (rulesTextObject == null)
        {
            Debug.Log("RulesText not found");
        }

        rulesText = rulesTextObject.GetComponent<TextMeshProUGUI>();
        if (rulesText == null)
        {
            Debug.Log("RulesText doesn't have a TextMeshProUGUI component");
        }

        DayRulesString1 = "Ceci est une molécule de dioxyde de carbone (CO2). On la trouve dans l’air, tout autour de nous, mais elle est minuscule et invisible à l’oeil nu.";
        DayRulesString2 = "En présence de lumière, ta plante a besoin d’absorber cette molécule pour produire du glucose qui lui permet de grandir et de se développer correctement.";
        DayRulesString3 = "Clique sur les feuilles lorsque la molécule passe devant elles pour la récolter.\n\nLa jauge te permettra de déterminer les besoins en CO2 de ta plante. Prends-en bien soin.";
        NightRulesString = "La nuit tombe ! En absorbant de l’oxygène (O2), la plante va poursuivre sa respiration pour utiliser ce qu’elle a récolté en journée et produire de l’énergie.\nA toi de jouer pour récupérer des molécules d’O2 afin que ta plante puisse grandir !";
        GlucoseRulesString = "Une nouvelle jauge vient d’apparaître : c’est celle du glucose. \nLa plante crée ce glucose grâce au dioxyde de carbone (CO2) et à l’oxygène (O2) que tu as récolté avant, et elle en a besoin pour vivre. \nNe laisse pas la jauge descendre à 0 !";

        StartCoroutine(DisplayRules(DayRulesString1));
    }

    // Update is called once per frame
    void Update()
    {
        if (timeManager.jour == 0 && timeManager.nuit && !precedenteNuit)
        {
            StartCoroutine(DisplayRules(NightRulesString));
        }
        if (timeManager.jour == 1 && precedentJour == 0)
        {
            StartCoroutine(DisplayRules(GlucoseRulesString));
        }
        precedenteNuit = timeManager.nuit;
        precedentJour = timeManager.jour;
    }

    IEnumerator DisplayRules(string rulesString)
    {
        yield return new WaitForSeconds(waitTime);

        Time.timeScale = 0;

        rulesText.SetText(rulesString);

        rulesRenderer.enabled = true;
        continueButtonRenderer.enabled = true;
        pauseButtonCanvas.SetActive(false);

        // Mettre les objets voulus au premier plan

        // Pour le jour
        if (ruleNumber == Rules.Day1)
        {
            // mettre CO2 au premier plan
            CO2Renderer.sortingOrder = 3;
            // mettre jauge CO2 au premier plan
            CO2Jauge.SetActive(true);
            Debug.Log("co2jauge");
        }

        // Pour la nuit
        else if (ruleNumber == Rules.Night)
        {   
            // On cherche l'O2 une fois qu'il existe
            GameObject O2 = GameObject.Find("O2(Clone)");
            if (O2 == null)
            {
                Debug.Log("O2 not found");
            }
            O2Renderer = O2.GetComponent<Renderer>();

            // mettre O2 au 1er plan
            O2Renderer.sortingOrder = 3;
            // mettre jauge O2 au premier plan
            O2Jauge.SetActive(true);
        }

        // Pour le glucose
        else if (ruleNumber == Rules.Glucose)
        {
            // mettre jauge glucose au 1er plan
            GlucoseJauge.SetActive(true);
        }
        
    }

    void ContinueOnClick()
    {
        // On affiche trois écrans d'affilée pour le jour
        if (ruleNumber == Rules.Day1)
        {
            rulesText.SetText(DayRulesString2);
        }
        else if (ruleNumber == Rules.Day2)
        {
            rulesText.SetText(DayRulesString3);
        }

        else 
        {
            Time.timeScale = 1;
            rulesRenderer.enabled = false;
            continueButtonRenderer.enabled = false;
            pauseButtonCanvas.SetActive(true);

            // Mettre tous au 2nd plan
            if (ruleNumber == Rules.Day3)
            {
                // CO2 2nd plan
                CO2Renderer.sortingOrder = 1;
                // CO2Jauge 2nd plan
                CO2Jauge.SetActive(false);
            }

            else if (ruleNumber == Rules.Night)
            {
                // O2 2nd plan
                O2Renderer.sortingOrder = 1;
                // O2Jauge 2nd plan
                O2Jauge.SetActive(false);
            }

            if (ruleNumber == Rules.Glucose)
            {
                // GlucoseJauge 2nd plan
                GlucoseJauge.SetActive(false);
            }
        }

        ruleNumber ++;
    }

}
