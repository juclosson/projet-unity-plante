using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Game_over : MonoBehaviour
{
    public GameObject gameOverPanel;
    public TMP_Text gameOverText;
    public Button restartButton;

    private GameObject BarresCanvas;
    private GameObject PauseButton;
    private GameObject TitleText;
    private GameObject MenuButton;

    private LevelManager levelManager;
    private int level;



    void Start()
    {
        GameObject LevelManagerObject = GameObject.Find("LevelManager(Clone)");
        if (LevelManagerObject == null)
        {
            Debug.Log("levelManager not found");
        }

        levelManager = LevelManagerObject.GetComponent<LevelManager>();
        level = levelManager.level;

        // Assurez-vous que le panneau de game over est desactive au debut.
        gameOverPanel.SetActive(false);
        gameOverText.gameObject.SetActive(false);
        restartButton.gameObject.SetActive(false);

        restartButton.onClick.AddListener(RestartLevel);

        if (level == 1)
        {
            BarresCanvas = GameObject.Find("BarresCanvas(Clone)");
        }
        else if (level == 2 || level == 3)      // A voir
        {
            BarresCanvas = GameObject.Find("BarresCanvas chap2(Clone)");
        }

        if (BarresCanvas == null)
        {
            Debug.Log("BarresCanvas not found");
        }
        
        PauseButton = GameObject.Find("PauseButton");

        if (PauseButton == null)
        {
            Debug.Log("PauseButton not found");
        }

        TitleText = GameObject.Find("Canvas_gameover(Clone)/TitleText");

        if (TitleText == null)
        {
            Debug.Log("TitleText not found");
        }
        
        MenuButton = GameObject.Find("Canvas_gameover(Clone)/MenuButton");
        if (MenuButton == null)
        {
            Debug.Log("MenuButton not found");
        }
        MenuButton.SetActive(false);
        MenuButton.GetComponent<Button>().onClick.AddListener(MenuOnClick);

        TitleText.SetActive(false);
    }

    public void GameOver()
    {
        Debug.Log("game-over_appele");
        // Arretez le jeu (facultatif, depend de votre logique de jeu).
        Time.timeScale = 0f;

        // Activez le panneau de game over et le texte
        gameOverPanel.SetActive(true);
        gameOverText.gameObject.SetActive(true);
        restartButton.gameObject.SetActive(true);
        MenuButton.SetActive(true);
        TitleText.SetActive(true);

        BarresCanvas.SetActive(false);
        PauseButton.SetActive(false);

        // Mettez a jour le texte du game over si necessaire.
        gameOverText.text = "Game Over :\n Vous avez perdu par manque de glucose";
        if (level==1)
        {
            gameOverText.text = "C’EST PERDU : \nLa plante n’a pas pu se développer correctement par manque de glucose.";
        }
        else
        {
            if (level >= 2)
            {
                gameOverText.text = "C’EST PERDU : \nLa plante n’a pas pu se développer correctement par manque " + MoleculeManager2.LoseReason;

            }
        }
        
        Canvas canvas = gameOverPanel.GetComponent<Canvas>();
        if (canvas != null)
        {
            canvas.sortingOrder = 999; // Utilisez une valeur appropriee ici.
        }
    }


    public void RestartLevel()
    {
        // Reactivez le temps ecoule pour que le jeu puisse etre joue a nouveau.
        Debug.Log("le bouton restart a ete presse"); //pas encore appele...
        Time.timeScale = 1f;

        // Rechargez la scene actuelle pour recommencer le niveau.(rappel les fonctions starts)
        UnityEngine.SceneManagement.SceneManager.LoadScene(UnityEngine.SceneManagement.SceneManager.GetActiveScene().name);
    }

    public void MenuOnClick()
    {
        Time.timeScale = 1;
        UnityEngine.SceneManagement.SceneManager.LoadScene("Main_menu");
    }

}
