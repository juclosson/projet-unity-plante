using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class WinManager : MonoBehaviour
{
    private TimeManager timeManager;
    [SerializeField]
    private GameObject WinCanvasPrefab;
    private GameObject pauseButtonCanvas;
    private GameObject BarresCanvas;
    private int precedentJour = 0;
    private GameObject WinCanvas;
    [SerializeField]
    private int nbJours = 2;
    private GameObject continueButton;
    private GameObject playAgainButton;
    private GameObject menuButton;

    private TextMeshProUGUI winText;

    private LevelManager levelManager;
    private int level;

    // Start is called before the first frame update
    void Start()
    {
        GameObject LevelManagerObject = GameObject.Find("LevelManager(Clone)");
        if (LevelManagerObject == null)
        {
            Debug.Log("levelManager not found");
        }

        levelManager = LevelManagerObject.GetComponent<LevelManager>();
        level = levelManager.level;

        GameObject timeManagerObject = GameObject.Find("TimeManager(Clone)");
        timeManager = timeManagerObject.GetComponent<TimeManager>();

        pauseButtonCanvas = GameObject.Find("PauseButtonCanvas(Clone)");
        
        if (level == 1)
        {
            BarresCanvas = GameObject.Find("BarresCanvas(Clone)");
        }
        else if (level == 2 || level == 3)      // Modifier si on a fait une version pour le 3 (sinon pas la peine)
        {
            BarresCanvas = GameObject.Find("BarresCanvas chap2(Clone)");
        }

        WinCanvas = Instantiate(WinCanvasPrefab);

        GameObject continueButton = GameObject.Find("WinCanvas(Clone)/ContinueButton");
        if (continueButton == null)
        {
            Debug.Log("continue Button not found");
        }
        continueButton.GetComponent<Button>().onClick.AddListener(ContinueOnClick);

        if (level == levelManager.lastLevel)
        {
            continueButton.SetActive(false);
        }

        GameObject playAgainButton = GameObject.Find("WinCanvas(Clone)/PlayAgainButton");
        playAgainButton.GetComponent<Button>().onClick.AddListener(PlayAgainOnClick);

        GameObject menuButton = GameObject.Find("WinCanvas(Clone)/MenuButton");
        menuButton.GetComponent<Button>().onClick.AddListener(MenuOnClick);

        // Texte
        GameObject winTextObject = GameObject.Find("winText");
        if (winTextObject == null)
        {
            Debug.Log("winText not found");
        }

        winText = winTextObject.GetComponent<TextMeshProUGUI>();
        if (winText == null)
        {
            Debug.Log("winText doesn't have a TextMeshProUGUI component");
        }

        if (level == levelManager.lastLevel)
        {
            winText.SetText("Bravo, tu as réussi le niveau " + level + " !\n Tu as su maintenir ta plante en forme jusqu'au bout de cette aventure !\n");
        }
        else 
        {
            winText.SetText("Bravo, ta plante est en forme ! Tu as réussi le niveau " + level + " !");
        }
        
        WinCanvas.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (timeManager.jour == nbJours && precedentJour == nbJours - 1)
        {
            pauseButtonCanvas.SetActive(false);
            BarresCanvas.SetActive(false);
            WinCanvas.SetActive(true);
            //winText.SetText("Bravo ! Tu as réussi le niveau " + level + " !");
            Time.timeScale = 0;
        }

        precedentJour = timeManager.jour;
    }

    void ContinueOnClick()
    {
        Time.timeScale = 1f;
        if (level == 1)
        {
            UnityEngine.SceneManagement.SceneManager.LoadScene("Niveau_2");
        }
        else if (level == 2)
        {
            UnityEngine.SceneManagement.SceneManager.LoadScene("Niveau_3");
        }
        
    }

    void PlayAgainOnClick()
    {
        Time.timeScale = 1f;
        UnityEngine.SceneManagement.SceneManager.LoadScene(UnityEngine.SceneManagement.SceneManager.GetActiveScene().name);
    }

    void MenuOnClick()
    {
        Time.timeScale = 1f;
        UnityEngine.SceneManagement.SceneManager.LoadScene("Main_menu");
    }
}
