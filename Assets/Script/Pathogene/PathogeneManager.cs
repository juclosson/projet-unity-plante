using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PathogeneManager : MonoBehaviour
{
    [SerializeField]
    private GameObject PathogenePrefab;
    [SerializeField]
    private float spawnRate = 12f;

    // Coordonnées des botus des racines
    private float[] xSpawnPosition = {-4.6f, -2.3f, 1.7f, 4.2f};
    private float[] ySpawnPosition = {-8.2f, -8.8f, -8.9f, -8.6f};
    private bool[] isSpawnable = {true, true, true, true};

    [SerializeField]
    private GameObject RacineMaskCanvasPrefab;
    [SerializeField]
    private GameObject RacineMaladePrefab;

    // Start is called before the first frame update
    void Start()
    {
        GameObject RacineMaskCanvas = Instantiate(RacineMaskCanvasPrefab);
        GameObject RacineMalade = Instantiate(RacineMaladePrefab);
        StartCoroutine(PathogeneSpawnCoroutine());
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    IEnumerator PathogeneSpawnCoroutine()
    {
        while (true)
        {
            if (anySpace())   // une racine est libre
            {
                int indice = WhereToSpawn();
                GameObject Pathogene = Instantiate(PathogenePrefab, new Vector3(xSpawnPosition[indice], ySpawnPosition[indice], 0), Quaternion.identity);
                Pathogene.GetComponent<Pathogene>().indice = indice;

                isSpawnable[indice] = false;
            }
            
            yield return new WaitForSeconds(spawnRate);
        }
    }

    bool anySpace()
    {
        bool space = false;

        foreach(bool spawn in isSpawnable)
        {
            if (spawn)
            {
                space = true;
            }
        }

        return space;
    }

    // Choisit le numero du point ou le pathogene va spawn s'il y en a un
    int WhereToSpawn()
    {
        List<int> indices = new List<int>();  // Tableau qui stocke les indices des points disponibles
        int taille = 0;

        int i;

        for (i = 0; i < 4; i++)
        {
            if (isSpawnable[i])
            {
                indices.Add(i);
                taille ++;
            }
        }

        return indices[Random.Range(0, taille)];
    }
}
