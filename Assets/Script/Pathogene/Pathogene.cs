using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pathogene : MonoBehaviour
{
    // Coordonnées du point d'arrivée des pathogènes dans le tronc
    private float xTronc = 0f;
    private float yTronc = -5.1f;

    private float vitesse = 0.2f;

    // Direction du pathogène dans son premier et son second segment
    private float xDirection1;
    private float xDirection2;
    private float yDirection1;
    private float yDirection2;

    // Indice du pathogène pour savoir sur quelle racine il se trouve
    public int indice;

    // Coordonnées du point intermédiaire de chaque trajectoire de pathogène
    private float[] xIntermediaire = {-2.8f, -1.2f, 1.1f, 3.4f};
    private float[] yIntermediaire = {-6.6f, -7.2f, -7.9f, -7.1f};

    private GameObject Mask;

    // Différence entre le centre du mask et son extrémité
    private float xMaskDelta;
    private float yMaskDelta;

    // Start is called before the first frame update
    void Start()
    {
        // Premier segment
        xDirection1 = (xIntermediaire[indice] - transform.position.x) * vitesse;
        yDirection1 = (yIntermediaire[indice] - transform.position.y) * vitesse;

        // Deuxième segment
        xDirection2 = (xTronc - xIntermediaire[indice]) * vitesse;
        yDirection2 = (yTronc - yIntermediaire[indice]) * vitesse;

        Mask = GameObject.Find("RacineMask" + (indice + 1));    // On récupère le mask correspondant

        // Valeurs de décalage de x et y entre le centre du mask et son extrémité
        if (indice == 0)
        {
            xMaskDelta = 3.5f;
            yMaskDelta = 1.8f;
        }
        else if (indice == 1)
        {
            xMaskDelta = 1.5f;
            yMaskDelta = -4.6f;
        }
        else if (indice == 2)
        {
            xMaskDelta = -0.8f;
            yMaskDelta = 2.3f;
        }
        else if (indice == 3)
        {
            xMaskDelta = -2.8f;
            yMaskDelta = 2.1f;
        }

    }

    // Update is called once per frame
    void Update()
    {
        // Déplacement du pathogène
        if (transform.position.y < yIntermediaire[indice])
        {
            transform.position += new Vector3(xDirection1 * Time.deltaTime , yDirection1 * Time.deltaTime, 0);
        }
        else if (transform.position.y < yTronc)
        {
            transform.position += new Vector3(xDirection2 * Time.deltaTime , yDirection2 * Time.deltaTime, 0);
        }

        //Déplacement du mask en fonction
        Mask.transform.position = new Vector3(transform.position.x - xMaskDelta, transform.position.y - yMaskDelta, 0);
    }
}
