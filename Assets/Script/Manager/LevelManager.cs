using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelManager : MonoBehaviour
{
    public int level;
    public int lastLevel = 2;   // Indique le dernier niveau terminé, sert à faire une page de victoire différente

    // Start is called before the first frame update
    void Start()
    {
        string name = SceneManager.GetActiveScene().name;

        if (name == "Niveau_1")
        {
            level = 1;
        }
        else if (name == "Niveau_2")
        {
            level = 2;
        }
        else if (name == "Niveau_3")
        {
            level = 3;
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
