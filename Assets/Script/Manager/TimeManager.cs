using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TimeManager : MonoBehaviour
{
    private float timer = 0;

    // Variables mises à disposition des autres scripts
    public  int jour = 0;   // Numéro du jour (comprend le jour et la nuit)
    public  bool nuit = false;

    public float dureeJour = 10.0f;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        timer += Time.deltaTime;

        if (timer > dureeJour/2)
        {
            nuit = true;
        }

        if (timer > dureeJour && nuit)
        {
            nuit = false;
            jour ++;
            timer = 0;
        }

    }
}
