using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Background : MonoBehaviour
{
    Image image;
    Color colorNuit;
    Color colorJour;
    public GameObject timeManagerObject;
    public TimeManager timeManager;
    public float fadeDuration = 1f;
    bool precedanteNuit = false;

    // Start is called before the first frame update
    void Start()
    {
        GameObject timeManagerObject = GameObject.Find("TimeManager(Clone)");
        timeManager = timeManagerObject.GetComponent<TimeManager>();
        image = GetComponent<Image>();
        colorNuit = new Color32(150, 170, 255, 255);
        colorJour = Color.white;
    }

    // Update is called once per frame
    void Update()
    {
        if (timeManager.nuit && !precedanteNuit)
        {
            StartCoroutine(FadeColor(colorJour, colorNuit));
        }
        else if (!timeManager.nuit && precedanteNuit)
        {
            StartCoroutine(FadeColor(colorNuit, colorJour));
        }
        
        precedanteNuit = timeManager.nuit;  // Permet de savoir lorsqu'on passe du jour à la nuit et inversement
    }

    // Transition en fondu entre startColor et endColor
    IEnumerator FadeColor(Color startColor, Color endColor)
    {
        float fadeTimer = 0f;

        while (fadeTimer < fadeDuration)
        {
            image.color = Color.Lerp(startColor, endColor, fadeTimer/fadeDuration);
            fadeTimer += Time.deltaTime;
            yield return fadeTimer;
        }
    }
}
