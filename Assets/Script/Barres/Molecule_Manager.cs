using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class MoleculeManager : MonoBehaviour
{
    [SerializeField]
    private int initGlucoseCount = 5;
    private static int co2Count = 0;
    private static int o2Count = 0;
    private static int glucoseCount = 5;

    [SerializeField]
    private float decrease_rate = 5f;
    [SerializeField]
    private float transformation_rate = 3f;

    public int stop = 1;
    public Game_over GameOver;

    public static int Co2Count
    {
        get { return co2Count; }
        set { co2Count = value >= 0 ? value : 0; }
    }

    public static int O2Count
    {
        get { return o2Count; }
        set { o2Count = value >= 0 ? value : 0; }
    }

    public static int GlucoseCount
    {
        get { return glucoseCount; }
        set { glucoseCount = value >= 0 ? value : 0; }
    }


    public static void IncrementCo2Count()
    {
        co2Count++;
    }

    public static void IncrementO2Count()
    {
        o2Count++;
    }


    private bool gameIsOver = false;
    public GameObject gameOverScreen;
    public GameObject glucose_barre;
    public GameObject o2_barre;


    private bool debutNuit = true;
    private bool debutJour = true;

    private TimeManager timeManager;




    private void Start()
    {
        GameObject GameOverObject = GameObject.Find("Canvas_gameover(Clone)");

        GameObject timeManagerObject = GameObject.Find("TimeManager(Clone)");
        timeManager = timeManagerObject.GetComponent<TimeManager>();


        if (GameOverObject == null)
        {
            Debug.Log("Canvas_gameover not found");
        }

        GameOver = GameOverObject.GetComponent<Game_over>();

        //StartCoroutine(DecreaseCountsglucoseRoutine());
        //StartCoroutine(TransformationcountRoutine());
        co2Count = 0;
        o2Count = 0;
        glucoseCount = initGlucoseCount;

        // gestion des barres
        glucose_barre.SetActive(false);
        o2_barre.SetActive(false);

    }

    private void Update()
    {
       
       
        //ok le compteur n'augmente pas.. comme celui de time manager normal
        // il faut arriver a récupérer la vrai valeur de temps(la on n'a pas la vrai valeur de time de TimeManager)

        //partie pour affichage des barres de molécules
        if (timeManager.jour == 0 && timeManager.nuit == true&& debutNuit)
        {
            o2_barre.SetActive(true); 
            
           
            debutNuit=false;

        }
        if (timeManager.jour == 1 && debutJour )
        {
            glucose_barre.SetActive(true);
            StartCoroutine(TransformationcountRoutine());
            StartCoroutine(DecreaseCountsglucoseRoutine());
            debutJour=false;
        }


  

    }











    //gestion des decroissances ici
    private System.Collections.IEnumerator DecreaseCountsglucoseRoutine()
    {
        while (true)
        {
            yield return new WaitForSeconds(decrease_rate);

            if (!gameIsOver)
            {
                glucoseCount--;


                // Verifier si l'un des compteurs est a 0
                if (glucoseCount<=0)
                {
                    Debug.Log("DEFAITE PAR MANQUE DE  GLUCOSE !");
                    stop = 0;
                    // Afficher la fen�tre "Game Over"
                    GameOver.GameOver();
                }
            }
        }
    }
    private System.Collections.IEnumerator TransformationcountRoutine()
    {
        while (true)
        {
            yield return new WaitForSeconds(transformation_rate);

            if (!gameIsOver)
            {
                if ( co2Count >=1 && o2Count >=1) // si on peut on récupère du glucose
                { 
                    Co2Count--;
                    o2Count--;
                    glucoseCount++;
                }

                // V�rifier si l'un des compteurs est � 0
                /*
                if (Co2Count == 0 || O2Count == 0)
                {
                    // Afficher la fen�tre "Game Over"
                    if (stop !=0) GameOver.GameOver();
                }
                */
            }
        }
    }
}
