using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BarresCanvas : MonoBehaviour
{
    private CameraDeplacement cameraDeplacement;

    // Start is called before the first frame update
    void Start()
    {
        GameObject MainCamera = GameObject.Find("Main Camera(Clone)");
        cameraDeplacement = MainCamera.GetComponent<CameraDeplacement>();
    }

    // Update is called once per frame
    void Update()
    {
        // Deplacement vertical pour suivre la camera
        transform.Translate(Vector3.up * cameraDeplacement.deplacementVertical * Time.deltaTime);
    }
}
