using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class MoleculeManager2 : MonoBehaviour
{
    [SerializeField]
    private int initGlucoseCount = 5;
    private static int co2Count = 0;
    private static int o2Count = 0;
    private static int h2oCount = 5;
    private static float glucoseCount = 5;

    [SerializeField]
    private float glucose_decrease_rate = 5f;
    [SerializeField]
    private float h2o_decrease_rate = 1f;
    [SerializeField]
    private float transformation_rate = 3f;


    public int stop = 1;
    public Game_over GameOver;
    private static string lose="";


    public static float valeur_nutriment=0.5f;

    public static int Co2Count
    {
        get { return co2Count; }
        set { co2Count = value >= 0 ? value : 0; }
    }

    public static int O2Count
    {
        get { return o2Count; }
        set { o2Count = value >= 0 ? value : 0; }
    }

    public static float GlucoseCount
    {
        get { return glucoseCount; }
        set { glucoseCount = value >= 0 ? value : 0; }
    }
    public static int H2oCount
    {
        get { return h2oCount; }
        set { h2oCount = value >= 0 ? value : 0; }
    }
    public static string LoseReason
    {
        get { return lose; }
 
    }

    public static void IncrementCo2Count()
    {
        co2Count++;
    }

    public static void IncrementO2Count()
    {
        o2Count++;
    }

    public static void IncrementH2OCount()
    {
        h2oCount++;
    }

    public static void RecupNutrimentt()
    {
        glucoseCount += valeur_nutriment;
    }

    public static void IncrementglucoseCount()
    {
        glucoseCount++;
    }



    private bool gameIsOver = false;
    public GameObject gameOverScreen;
    public GameObject glucose_barre;
    public GameObject o2_barre;
    public GameObject h2o_barre;


    //private bool debutNuit = true;
    //private bool debutJour = true;

    private TimeManager timeManager;




    private void Start()
    {
        GameObject GameOverObject = GameObject.Find("Canvas_gameover(Clone)");

        GameObject timeManagerObject = GameObject.Find("TimeManager(Clone)");
        timeManager = timeManagerObject.GetComponent<TimeManager>();


        if (GameOverObject == null)
        {
            Debug.Log("Canvas_gameover not found");
        }

        GameOver = GameOverObject.GetComponent<Game_over>();

        StartCoroutine(DecreaseCountsglucoseRoutine2());
        StartCoroutine(TransformationcountRoutine2());
        StartCoroutine(DecreaseCountsH2oRoutine2());

        co2Count = 5;
        o2Count  = 5;
        h2oCount = 5;
        glucoseCount = initGlucoseCount;



       
    }

    private void Update()
    {

    
    }











    //gestion des decroissances ici
    private System.Collections.IEnumerator DecreaseCountsglucoseRoutine2()
    {
        while (true)
        {
            yield return new WaitForSeconds(glucose_decrease_rate);

            if (!gameIsOver)
            {
                glucoseCount--;


                // Verifier si l'un des compteurs est a 0
                if (glucoseCount<=0)
                {
                    //Debug.Log("DEFAITE PAR MANQUE DE  GLUCOSE !");
                    stop = 0;
                    lose = "de glucose.";
                    // Afficher la fen�tre "Game Over"
                    GameOver.GameOver();
                }
            }
        }
    }
    private System.Collections.IEnumerator TransformationcountRoutine2()
    {
        while (true)
        {
            yield return new WaitForSeconds(transformation_rate);

            if (!gameIsOver)
            {
                if ( co2Count >=1 && o2Count >=1) // si on peut on récupère du glucose
                { 
                    Co2Count--;
                    o2Count--;
                    glucoseCount++;
                }
            }
        }
    }
    private System.Collections.IEnumerator DecreaseCountsH2oRoutine2()
    {
        while (true)
        {
            yield return new WaitForSeconds(h2o_decrease_rate);

            if (!gameIsOver)
            {
                h2oCount--;
                //Debug.Log(h2oCount);


                // Verifier si l'un des compteurs est a 0
                if (h2oCount <= 0)
                {
                    //Debug.Log("DEFAITE PAR MANQUE DE  WATER !");
                    stop = 0;
                    lose = "d'eau (H2O).";
                    // Afficher la fen�tre "Game Over"
                    GameOver.GameOver();
                }
            }
        }
    }
}



