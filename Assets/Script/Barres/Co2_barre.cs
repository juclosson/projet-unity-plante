using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class Co2_barre : MonoBehaviour
{
    public Slider barreDeChargement; // R�f�rence � votre composant Slider dans l'interface Unity
    int capacite_max = 10;

    private void Start()
    {
        MiseAJourBarreDeChargement();
    }

    private void MiseAJourBarreDeChargement()
    {
        // vérification d'initialisation
        if ( barreDeChargement != null)
        {
            float pourcentage = (float)MoleculeManager.Co2Count / capacite_max; // Adapter selon vos besoins

            // Mettre � jour la barre de chargement
            barreDeChargement.value = pourcentage;
        }
        else
        {
            Debug.LogError("Assurez-vous que MoleculeManager est correctement initialis� et que la r�f�rence de la barre de chargement est d�finie.");
        }
    }

    // Vous pouvez appeler cette fonction lorsque le nombre d'objets (mol�cules de CO2) change
    public void Update()
    {
        MiseAJourBarreDeChargement();
    }
}
