using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Niveau1Manager : MonoBehaviour
{
    [SerializeField]
    private GameObject LevelManagerPrefab;
    [SerializeField]
    private GameObject TimeManagerPrefab;

    [SerializeField]
    private GameObject MainCameraPrefab;

    [SerializeField]
    private GameObject CO2ManagerPrefab;
    [SerializeField]
    private GameObject O2ManagerPrefab;

    [SerializeField]
    private GameObject SolPrefab;
    [SerializeField]
    private GameObject BackgroundCanvasPrefab;

    [SerializeField]
    private GameObject ChenePrefab;

    [SerializeField]
    private GameObject PausePrefab;
    [SerializeField]
    private GameObject RulesManagerPrefab;
    [SerializeField]
    private GameObject CanvasGameOverPrefab;
    [SerializeField]
    private GameObject CanvasBarrePrefab;
    [SerializeField]
    private GameObject WinManagerPrefab;

    // Start is called before the first frame update
    void Start()
    {
        GameObject LevelManager = Instantiate(LevelManagerPrefab);
        GameObject TimeManager = Instantiate(TimeManagerPrefab);

        GameObject MainCamera = Instantiate(MainCameraPrefab);

        GameObject CO2Manager = Instantiate(CO2ManagerPrefab);
        GameObject O2Manager = Instantiate(O2ManagerPrefab);

        GameObject Sol = Instantiate(SolPrefab);
        GameObject Background = Instantiate (BackgroundCanvasPrefab);

        GameObject Chene = Instantiate(ChenePrefab);

        GameObject Pause = Instantiate(PausePrefab);
        GameObject RulesManager = Instantiate(RulesManagerPrefab);
        GameObject CanvasGameOver = Instantiate(CanvasGameOverPrefab);
        GameObject CanvasBarre = Instantiate(CanvasBarrePrefab);
        GameObject WinManager = Instantiate(WinManagerPrefab);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
