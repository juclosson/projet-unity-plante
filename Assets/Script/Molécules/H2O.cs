using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class H2O : MonoBehaviour
{
    [SerializeField]
    private float y0 = -1.0f;
    private float x0;
    [SerializeField]
    private float vitesse = 0.5f;
    private Vector2 target;
    [SerializeField]
    private float lifetime = 10.0f;
    private float timer = 0;
    private Rigidbody2D rb;
    [SerializeField]
    private float xmax = 1;

    void Start()
    {
        rb = gameObject.GetComponent<Rigidbody2D>();

        // Abscisse aléatoire
        x0 = Random.Range(-xmax, xmax);

        Vector2 direction = new Vector2(0, -1);

        transform.position = new Vector2(x0, y0);

        rb.velocity = direction * vitesse;    // Pas de deltatime car c'est le moteur qui calcule
    }

    // Update is called once per frame
    void Update()
    {
        timer += Time.deltaTime;

        if (timer > lifetime)
        {
            Destroy(gameObject);
        }
    }
}
