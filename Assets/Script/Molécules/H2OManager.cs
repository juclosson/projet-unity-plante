using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class H2OManager : MonoBehaviour
{
    [SerializeField]
    private GameObject H2OPrefab;
    public float spawnRate = 2;
    private TimeManager timeManager;

    GameObject Pluie;
    [SerializeField]
    private float pluieCycleDuration = 10.0f;
    private float timer;

    // Start is called before the first frame update
    void Start()
    {
        GameObject timeManagerObject = GameObject.Find("TimeManager(Clone)");
        timeManager = timeManagerObject.GetComponent<TimeManager>();

        Pluie = GameObject.Find("canvas pluie(Clone)");

        StartCoroutine(H2OSpawnCoroutine());
    }

    // Update is called once per frame
    void Update()
    {
        timer += Time.deltaTime;
    }

    IEnumerator H2OSpawnCoroutine()
    {
        while (true)
        {
            timer = 0f;

            // Période de pluie
            
            Pluie.SetActive(true);

            while (timer < pluieCycleDuration)
            {
                GameObject H2O = Instantiate(H2OPrefab, Vector3.zero, Quaternion.identity);

                yield return new WaitForSeconds(spawnRate);
            }

            //Période sans pluie

            Pluie.SetActive(false);
            
            yield return new WaitForSeconds(pluieCycleDuration);
        }
    }
}
