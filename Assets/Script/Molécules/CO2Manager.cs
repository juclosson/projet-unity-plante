using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CO2Manager : MonoBehaviour
{
    [SerializeField]
    private GameObject CO2Prefab;
    private List<GameObject> CO2List = new List<GameObject>();
    public float spawnRate = 1;
    private TimeManager timeManager;

    // Start is called before the first frame update
    void Start()
    {
        GameObject timeManagerObject = GameObject.Find("TimeManager(Clone)");
        timeManager = timeManagerObject.GetComponent<TimeManager>();

        StartCoroutine(CO2SpawnCoroutine());
    }

    // Update is called once per frame
    void Update()
    {

    }

    IEnumerator CO2SpawnCoroutine()
    {
        while (true)
        {
            if (!timeManager.nuit)
            {
                // Instantiation d'un CO2, il définira sa position lui-même
                GameObject CO2 = Instantiate(CO2Prefab, Vector3.zero, Quaternion.identity);

                CO2List.Add(CO2);
            }
            
            yield return new WaitForSeconds(spawnRate);
        }
    }
}
