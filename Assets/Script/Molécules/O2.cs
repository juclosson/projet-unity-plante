using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class O2 : MonoBehaviour
{
    [SerializeField]
    private float y0 = 6.0f;
    private float x0, x1, y1;
    [SerializeField]
    private float vitesse = 1f;
    private Vector2 target;
    [SerializeField]
    private float lifetime = 10.0f;
    private float timer = 0;
    private Rigidbody2D rb;

    // Définissent un carré vers le milieu dans lequel passera la molécule
    [SerializeField]
    private float xmin = -1;
    [SerializeField]
    private float xmax = 1;
    [SerializeField]
    private float ymin = -1;
    [SerializeField]
    private float ymax = 1;

    private int level;

    void Start()
    {
        GameObject LevelManagerObject = GameObject.Find("LevelManager(Clone)");
        if (LevelManagerObject == null)
        {
            Debug.Log("levelManager not found");
        }

        LevelManager levelManager = LevelManagerObject.GetComponent<LevelManager>();
        level = levelManager.level;

        // La hauteur de l'écran varie en fonction du chapitre
        if (level == 1)
        {
            y0 = 6.0f;
        }
        else 
        {
            y0 = 11.0f;
        }

        rb = gameObject.GetComponent<Rigidbody2D>();

        // Abscisse d'apparition aléatoire
        x0 = Random.Range(-9.0f, 9.0f);

        // Point aléatoire dans le carré, la molécule passera par là
        x1 = Random.Range(xmin, xmax);
        y1 = Random.Range(ymin, ymax);

        Vector2 spawnPoint = new Vector2(x0, y0);
        Vector2 targetPoint = new Vector2(x1, y1);
        Vector2 direction = targetPoint - spawnPoint;

        direction.Normalize();

        transform.position = new Vector2(x0, y0);

        // La molécule est lancée dans une direction donnée puis le moteur s'occupe des calculs
        rb.velocity = direction * vitesse;
    }

    // Update is called once per frame
    void Update()
    {
        timer += Time.deltaTime;

        if (timer > lifetime)
        {
            Destroy(gameObject);
        }
    }
}
