using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class pluie : MonoBehaviour
{
    public float newAnimationSpeed = 0.8f; // Ajustez cette valeur selon vos besoins, c'est la frequence de changement

    void Start()
    {
        // Acc�dez au composant Animator
        Animator animator = GetComponent<Animator>();

        // V�rifiez si le composant Animator existe
        if (animator != null)
        {
            // Ajustez la vitesse de l'animation
            animator.speed = newAnimationSpeed;
        }
    }
}
