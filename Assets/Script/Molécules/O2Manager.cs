using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class O2Manager : MonoBehaviour
{
    [SerializeField]
    private GameObject O2Prefab;
    private List<GameObject> O2List = new List<GameObject>();
    public float spawnRate = 1;
    private TimeManager timeManager;

    // Start is called before the first frame update
    void Start()
    {
        GameObject timeManagerObject = GameObject.Find("TimeManager(Clone)");
        timeManager = timeManagerObject.GetComponent<TimeManager>();
        
        StartCoroutine(O2SpawnCoroutine());
    }

    // Update is called once per frame
    void Update()
    {

    }

    IEnumerator O2SpawnCoroutine()
    {
        while (true)
        {
            if (timeManager.nuit)
            {
                GameObject O2 = Instantiate(O2Prefab, Vector3.zero, Quaternion.identity);

                O2List.Add(O2);
            }
            
            yield return new WaitForSeconds(spawnRate);
        }
    }
}
