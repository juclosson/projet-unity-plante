using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;


public class Gestion_button : MonoBehaviour
{
    public Button button1;
    public Button button2;
    public Button button3;
    public Button buttonq;


    private void Start()
    {
        button1.onClick.AddListener(LancementChap1);
        button2.onClick.AddListener(LancementChap2);
        button3.onClick.AddListener(LancementChap3);
        buttonq.onClick.AddListener(quitter_jeu);
    }

    private void Update()
    {
        
    }


    void LancementChap1()
    {
        Debug.Log("lancement du chap1!");

        SceneManager.LoadScene("Niveau_1");
        //Debug.Log("Ah! cette fonctionalité n'est pas encore active!");

    }
    void LancementChap2()
    {
        Debug.Log("You have clicked the button chap 2!");

        SceneManager.LoadScene("Niveau_2");
    }
    void LancementChap3()
    {
        Debug.Log("lancement du chap3!");
        SceneManager.LoadScene("Niveau_3");

    }
    void quitter_jeu()
    {
        Debug.Log("on quite le jeu");
        Application.Quit();
    }


}
