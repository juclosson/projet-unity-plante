using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mvt_nuage : MonoBehaviour
{
    //public Sprite nuage_loin_s;
    //public Sprite nuage_proche_s;
  
   

    public GameObject nuages_loin;
    public GameObject nuages_proche;


    //positions des 2 groupes de nuages
    private float y0;
    private float x0;

    private float y1;
    private float x1;


   
    void Start()
    {
        //positionement initiales des 2 nuages:
        y0 = 3f;
        x0 = -5f;
        nuages_loin.transform.position = new Vector2(x0, y0);

        y1 = 3f;
        x0 = -5f;
        nuages_proche.transform.position = new Vector2(x1, y1);
    }

   
    void Update()
    {
        //on fait avancer les nuages:
        //nuages proches  ( donc plus rapides)
        if (nuages_proche.transform.position.x < 30)
        {
            nuages_proche.transform.position = nuages_proche.transform.position + new Vector3((float)((0.5)) * Time.deltaTime,0, 0);
        }
        else
        {
            nuages_proche.transform.position = new Vector3(-30f, 3f, 0);
        }

        //nuages loins (donc plus lents)

        if (nuages_loin.transform.position.x < 28)
        {
            nuages_loin.transform.position = nuages_loin.transform.position + new Vector3((float)((0.2) * Time.deltaTime), 0, 0);
        }
        else
        {
            nuages_loin.transform.position = new Vector3(-27f, 3f, 0);
        }


        //si l'un des groupe de nuage sort de l'�cran il r�aparait en boucle � gauche
    }
}
