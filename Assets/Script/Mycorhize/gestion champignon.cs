using System.Collections;
using System.Collections.Generic;
using UnityEngine;













//ceci est un fichier de test qui va afficher tout les champignon au fur et a mesure du temps, selon leur liste.
public class gestionchampignon : MonoBehaviour
{
    public GameObject Champignon_originel;
    public List<GameObject> liste_champi;
    public List<GameObject> liste_petit_champi;


    public int index_liste;
    private float timer = 0f;
    private float interval = 1f;

    void Start()
    {
        index_liste = 0;


        //d�sactivation de tout les objets initiaux
        for (int i = index_liste; i < liste_champi.Count; i++)
        {
            liste_champi[i].SetActive(false);
        }

        for (int i = index_liste; i < liste_petit_champi.Count; i++)
        {
            liste_petit_champi[i].SetActive(false);
        }


    }

    // Update is called once per frame
    void Update()
    {

        timer += Time.deltaTime;
        if (timer >= interval)
        {
            timer = 0f;

            // Activer le prochain GameObject s'il y en a un
            if (index_liste < liste_champi.Count)
            {
                liste_champi[index_liste].SetActive(true);
                liste_petit_champi[index_liste].SetActive(true);
                index_liste++;

                // Arr�tez la mise � jour si tous les GameObjects ont �t� activ�s
                if (index_liste >= liste_champi.Count)
                {
                    enabled = false; // D�sactiver ce script
                }
            }
        }
    }
}
